//
//  TodayViewController.m
//  widget
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "PomidorkaDto.h"
#import "PomidorkaService.h"
#import "Pomidorka.h"

@interface TodayViewController () <NCWidgetProviding> {
    Pomidorka *currentPomidorka;
}

@end

@implementation TodayViewController
@synthesize _timer, timerLabel, statusView;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.extensionContext setWidgetLargestAvailableDisplayMode:NCWidgetDisplayModeExpanded];
    
    _ticks = 0.0;
    
    currentPomidorka = nil;
    
    [self loadCurrentPomidorka];
    
    [self startTimer];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)widgetActiveDisplayModeDidChange:(NCWidgetDisplayMode)activeDisplayMode withMaximumSize:(CGSize)maxSize {
    if (activeDisplayMode == NCWidgetDisplayModeCompact){
        // Changed to compact mode
        self.preferredContentSize = maxSize;
    }
    else{
        // Changed to expanded mode
        self.preferredContentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 60);
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startPomidorka:(id)sender {
    
}

- (IBAction)abort:(id)sender {
    [[[NSThread alloc] initWithTarget:self
                             selector:@selector( abortPomidorkaFromEnd: )
                               object:nil ] start];
}

- (void)abortPomidorkaFromEnd:(id)sender {
    NSThread *thr = (NSThread *)sender;
    
    if (currentPomidorka) {
        PomidorkaDto *pd = [PomidorkaService abort:currentPomidorka.pid];
        NSLog(@"%@", currentPomidorka.pid);
        currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
    }
    
    [thr cancel];
}

- (void)loadCurrentPomidorka {
    [[[NSThread alloc] initWithTarget:self
                             selector:@selector( loadPomidorkaFromEnd: )
                               object:nil ] start];
}

- (void)loadPomidorkaFromEnd:(id)sender {
    PomidorkaDto *pd = [PomidorkaService current];
    NSLog(@"%u", pd.status);
    if (pd.status == RUNNING) {
        if (currentPomidorka) {
            if ((![currentPomidorka.pid isEqual:pd.pid])) {
                
                currentPomidorka = nil;
                currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
                
                NSLog(@"Pomidorka loaded");
            }
        } else {
            currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
            //NSLog(@"startTs = %ld, stopTs = %ld", pd.startTs, pd.stopTs);
        }
    } else {
        timerLabel.text = [NSString stringWithFormat:@"00:00"];
        currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
    }
}

- (void)startTimer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
}

- (void)timerTick:(NSTimer *)timer {
    _ticks += 1.0;
    
    if (currentPomidorka.status == RUNNING) {
        double seconds = [currentPomidorka howMuchTimeLeft];
        if (seconds <= 0.0) {
            timerLabel.text = [NSString stringWithFormat:@"00:00"];
            statusView.backgroundColor = [UIColor clearColor];
        } else {
            timerLabel.text = [NSString stringWithFormat:@"%.0f : %.0f", fmod(trunc(seconds / 60.0), 60.0), fmod(seconds, 60.0)];
            if (currentPomidorka.type == WORK) {
                statusView.backgroundColor = [UIColor redColor];
            } else {
                statusView.backgroundColor = [UIColor blueColor];
            }
        }
    } else {
        timerLabel.text = [NSString stringWithFormat:@"00:00"];
        statusView.backgroundColor = [UIColor clearColor];
    }
    
    if (fmod(_ticks, 5.0) == 0.0) {
        [self loadCurrentPomidorka];
    }
}

- (void)stopTimer {
    [_timer invalidate];
    _ticks = 0.0;
    _timer = nil;
}

@end

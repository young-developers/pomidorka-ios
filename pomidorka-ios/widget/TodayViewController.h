//
//  TodayViewController.h
//  widget
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayViewController : UIViewController {
    __weak NSTimer *_timer;
    CFTimeInterval _ticks;
}
    
@property (weak, nonatomic) NSTimer *_timer;

@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;

- (IBAction)abort:(id)sender;

@end

//
//  NoteDto.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NoteDto : NSObject {
    NSString *pid;
    bool isCompleted;
    long createTs;
    long lastUpdateTs;
    NSString *payload;
}

@property NSString *pid;
@property bool isCompleted;
@property long createTs;
@property long lastUpdateTs;
@property NSString *payload;

- (instancetype)initWithJSONString:(NSString *)JSONString;
- (instancetype)initWithDictionary:(NSDictionary *)JSONDictionary;

@end

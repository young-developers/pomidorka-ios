//
//  ViewController.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "ViewController.h"

#import "PomidorkaDto.h"
#import "PomidorkaService.h"
#import "Pomidorka.h"

#import "NoteService.h"


@interface ViewController() {
    Pomidorka *currentPomidorka;
}

@end

@implementation ViewController
@synthesize timerLabel, indicator, statusView, _timer, scrollView;
- (void)viewDidLoad {
    [super viewDidLoad];
    currentNotes = [[NSArray alloc] init];
    _ticks = 0.0;
    
    currentPomidorka = nil;
    
    indicator.hidden = YES;
    
    [indicator startAnimating];
    indicator.hidden = NO;
    
    [self loadCurrentPomidorka];
    [self loadNotes];
    
    [self startTimer];
    // Do any additional setup after loading the view, typically from a nib.
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateNote:(Note *)note {
    [NoteService update:note];
}

- (IBAction)addNote:(id)sender {
    AddNoteView *nv = [[AddNoteView alloc] initWithDelegate:self];
    nv.center = self.view.center;
    [self.view addSubview:nv];
}

- (void)addNoteFromView:(NSString *)str {
    [NoteService createNote:str];
}

- (IBAction)startPomidorka:(id)sender {
    
}

- (IBAction)abort:(id)sender {
    [[[NSThread alloc] initWithTarget:self
                             selector:@selector( abortPomidorkaFromEnd: )
                               object:nil ] start];
}

- (void)loadNotes {
    [[[NSThread alloc] initWithTarget:self
                             selector:@selector( loadNotesFromEnd: )
                               object:nil ] start];
}

- (void)loadNotesFromEnd:(id)sender {
    NSArray *arr = [NoteService notes];
    
    if ([currentNotes isEqualToArray:arr])
        return;
    else
        currentNotes = arr;
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createTs"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    arr = (NSMutableArray *)[arr sortedArrayUsingDescriptors:sortDescriptors];
    
    int i = 0;
    
    [scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    for (NoteDto *note in arr) {
        NoteView *nv = [[NoteView alloc] initWithNote:[[Note alloc] initWithNoteDto:note]];
        
        nv.frame = CGRectMake(0, nv.frame.size.height * i + 10 * i, scrollView.frame.size.width, nv.frame.size.height);
        [nv setMyDelegate:self];
        
        [self.scrollView addSubview:nv];
        self.scrollView.contentSize = CGSizeMake(0, (nv.frame.size.height + 10) * (i + 1));
        i++;
    }
}

- (void)abortPomidorkaFromEnd:(id)sender {
    NSThread *thr = (NSThread *)sender;
    
    if (currentPomidorka) {
        PomidorkaDto *pd = [PomidorkaService abort:currentPomidorka.pid];
        NSLog(@"%@", currentPomidorka.pid);
        currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
    }
    
    [thr cancel];
}

- (void)loadCurrentPomidorka {
    [[[NSThread alloc] initWithTarget:self
                             selector:@selector( loadPomidorkaFromEnd: )
                               object:nil ] start];
}

- (void)loadPomidorkaFromEnd:(id)sender {
    NSThread *thr = (NSThread *)sender;
    
    PomidorkaDto *pd = [PomidorkaService current];

    if (pd.status == RUNNING) {
        if (currentPomidorka) {
            if ((![currentPomidorka.pid isEqual:pd.pid])) {
                
                currentPomidorka = nil;
                currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
                
                NSLog(@"Pomidorka loaded");
            }
        } else {
            currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
            //NSLog(@"startTs = %ld, stopTs = %ld", pd.startTs, pd.stopTs);
        }
    } else {
        timerLabel.text = [NSString stringWithFormat:@"00 : 00"];
        currentPomidorka = [[Pomidorka alloc] initWithDto:pd];
    }
    [indicator stopAnimating];
    indicator.hidden = YES;
    [thr cancel];
}

- (void)startTimer {
    if (_timer == nil) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    }
}

- (void)timerTick:(NSTimer *)timer {
    _ticks += 1.0;
    
    if (currentPomidorka.status == RUNNING) {
        double seconds = [currentPomidorka howMuchTimeLeft];
        if (seconds <= 0.0) {
            timerLabel.text = [NSString stringWithFormat:@"00 : 00"];
            statusView.backgroundColor = [UIColor clearColor];
        } else {
            NSMutableString *timerStr = [[NSMutableString alloc] initWithString:@""];
            
            if (fmod(trunc(seconds / 60.0), 60) < 10) [timerStr appendString:[NSString stringWithFormat:@"0%.0f", fmod(trunc(seconds / 60.0), 60.0)]];
            else [timerStr appendString:[NSString stringWithFormat:@"%.0f", fmod(trunc(seconds / 60.0), 60.0)]];
            
            if (fmod(seconds, 60.0) < 10) [timerStr appendString:[NSString stringWithFormat:@" : 0%.0f", fmod(seconds, 60.0)]];
            else [timerStr appendString:[NSString stringWithFormat:@" : %.0f", fmod(seconds, 60.0)]];
            
            timerLabel.text = timerStr;
            
            if (currentPomidorka.type == WORK) {
                statusView.backgroundColor = [UIColor redColor];
            } else {
                statusView.backgroundColor = [UIColor blueColor];
            }
        }
    } else {
        timerLabel.text = [NSString stringWithFormat:@"00 : 00"];
        statusView.backgroundColor = [UIColor clearColor];
    }
    
    if (fmod(_ticks, 5.0) == 0.0) {
        [self loadCurrentPomidorka];
    }
    
    if (fmod(_ticks, 10.0) == 0) {
        [self loadNotes];
    }
}

- (void)stopTimer {
    [_timer invalidate];
    _ticks = 0.0;
    _timer = nil;
}

@end

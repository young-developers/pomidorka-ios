//
//  NoteService.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NoteDto.h"
#import "Note.h"

@interface NoteService : NSObject

+ (NSArray *)notes;
+ (NoteDto *)createNote:(NSString *)payload;
+ (NoteDto *)update:(Note *)noteParam;

@end

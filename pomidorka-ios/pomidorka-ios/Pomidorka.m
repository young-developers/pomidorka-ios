//
//  Pomidorka.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "Pomidorka.h"

@implementation Pomidorka
@synthesize pid, serverTs, startTs, stopTs, type, status;
- (id)initWithDto:(PomidorkaDto *)pomidorka {
    self = [super init];
    if ( self ) {
        pid = pomidorka.pid;
        serverTs = [NSDate dateWithTimeIntervalSince1970:pomidorka.serverTs / 1000];
        startTs = [NSDate dateWithTimeIntervalSince1970:pomidorka.startTs / 1000];
        stopTs = [NSDate dateWithTimeIntervalSince1970:pomidorka.stopTs / 1000];
        type = pomidorka.type;
        status = pomidorka.status;
    }
    return self;
}

- (double)howMuchTimeLeft{
    //NSLog(@"%ld", (long)[[NSDate date] timeIntervalSince1970]);
    return [self.stopTs timeIntervalSinceDate:[NSDate date]];
}

@end

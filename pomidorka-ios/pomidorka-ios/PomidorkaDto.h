//
//  PomidorkaDto.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enums.h"

/*typedef enum{
    WORK,
    REST
}TypeDto;

typedef enum {
    RUNNING,
    ABORTED,
    FINISHED
}StatusDto;*/

@interface PomidorkaDto : NSObject {
    NSString *pid;
    long serverTs;
    long startTs;
    long stopTs;
    TypeDto type;
    StatusDto status;
}

@property NSString *pid;
@property long serverTs;
@property long startTs;
@property long stopTs;
@property TypeDto type;
@property StatusDto status;

- (instancetype)initWithJSONString:(NSString *)JSONString;
- (id)initWithParams:(NSString *)idParam
                 and:(long)startTsParam
                 and:(long)stopTsParam
                 and:(TypeDto)typeParam
                 and:(StatusDto)statusParam;

- (NSString *)toJSON;
@end

//
//  ViewController.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddNoteView.h"
#import "NoteView.h"
#import "Note.h"

@interface ViewController : UIViewController <AddNoteDelegate, NoteDelegate> {
    NSArray *currentNotes;
    __weak NSTimer *_timer;
    CFTimeInterval _ticks;
}

@property (weak, nonatomic) NSTimer *_timer;

@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)addNote:(id)sender;

- (IBAction)startPomidorka:(id)sender;
- (IBAction)abort:(id)sender;
- (void)loadNotes;
- (void)addNoteFromView:(NSString *)str;
- (void)updateNote:(Note *)note;

@end


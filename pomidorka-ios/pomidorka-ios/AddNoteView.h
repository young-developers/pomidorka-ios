//
//  AddNoteView.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddNoteDelegate <NSObject>
- (void)loadNotes;
- (void)addNoteFromView:(NSString *)str;
@end

@interface AddNoteView : UIView {
    __weak id myDelegate;
}

@property (nonatomic, weak) id <AddNoteDelegate> myDelegate;
@property (weak, nonatomic) IBOutlet UITextField *noteField;

- (id)initWithDelegate:(id <AddNoteDelegate>)deleg;

- (IBAction)ok:(id)sender;

@end

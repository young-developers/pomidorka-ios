//
//  Note.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "Note.h"

@implementation Note
@synthesize pid, isCompleted, createTs, lastUpdateTs, payload;

- (id)initWithParams:(NSString *)idParam
                 and:(bool)isCompletedParam
                 and:(long)createTsParam
                 and:(long)lastUpdateTsParam
                 and:(NSString *)payloadParam {
    self = [super init];
    if (self) {
        pid = idParam;
        isCompleted = isCompletedParam;
        createTs = [NSDate dateWithTimeIntervalSince1970:createTsParam / 1000];
        lastUpdateTs = [NSDate dateWithTimeIntervalSince1970:lastUpdateTsParam / 1000];
        payload = payloadParam;
    }
    return self;
}

- (id)initWithNoteDto:(NoteDto *)note {
    self = [super init];
    if (self) {
        pid = note.pid;
        isCompleted = note.isCompleted;
        createTs = [NSDate dateWithTimeIntervalSince1970:note.createTs / 1000];
        lastUpdateTs = [NSDate dateWithTimeIntervalSince1970:note.lastUpdateTs / 1000];
        payload = note.payload;
    }
    return self;
}

@end

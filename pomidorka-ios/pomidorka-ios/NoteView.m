//
//  NoteView.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "NoteView.h"

@implementation NoteView
@synthesize completeButton, scrollView, payloadLabel, myDelegate;

- (id)initWithNote:(Note *)noteParam {
    self = [super init];
    if ( self ) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"NoteView"
                                              owner:self
                                            options:nil] lastObject];
        note = noteParam;
        
        if (note.isCompleted) {
            [completeButton setBackgroundImage:[UIImage imageNamed:@"completed.png"] forState:UIControlStateNormal];
            completeButton.enabled = NO;
        } else {
            [completeButton setBackgroundImage:[UIImage imageNamed:@"notCompleted.png"] forState:UIControlStateNormal];
        }
        
        payloadLabel.text = note.payload;
        
        CGSize textSize = [[payloadLabel text] sizeWithAttributes:@{NSFontAttributeName:[payloadLabel font]}];
        CGFloat width = textSize.width;
        self.payloadLabel.frame = CGRectMake(self.payloadLabel.frame.origin.x, self.payloadLabel.frame.origin.y, width, self.payloadLabel.frame.size.height);
        self.scrollView.contentSize = CGSizeMake( payloadLabel.frame.size.width, self.scrollView.frame.size.height);
        
    }
    return self;
}

- (IBAction)complete:(id)sender {
    note.isCompleted = YES;
    
    if (note.isCompleted) {
        [completeButton setBackgroundImage:[UIImage imageNamed:@"completed.png"] forState:UIControlStateNormal];
        completeButton.enabled = NO;
    }
    
    [myDelegate updateNote:note];
}
@end

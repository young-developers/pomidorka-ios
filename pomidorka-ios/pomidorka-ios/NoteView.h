//
//  NoteView.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Note.h"

@protocol NoteDelegate <NSObject>
- (void)updateNote:(Note *)note;
@end

@interface NoteView : UIView {
    __weak id myDelegate;
    Note *note;
}

@property (nonatomic, weak) id <NoteDelegate> myDelegate;
@property (weak, nonatomic) IBOutlet UIButton *completeButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *payloadLabel;

- (id)initWithNote:(Note *)noteParam;

- (IBAction)complete:(id)sender;

@end

//
//  PomidorkaDto.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "PomidorkaDto.h"
#import <objC/runtime.h>

@implementation PomidorkaDto
@synthesize pid, serverTs, startTs, stopTs, type, status;

- (id)initWithParams:(NSString *)idParam
                 and:(long)startTsParam
                 and:(long)stopTsParam
                 and:(TypeDto)typeParam
                 and:(StatusDto)statusParam {
    self = [super init];
    if (self) {
        pid = idParam;
        startTs = startTsParam;
        stopTs = stopTsParam;
        type = typeParam;
        status = statusParam;
    }
    return self;
}

- (instancetype)initWithJSONString:(NSString *)JSONString
{
    self = [super init];
    if (self) {
        
        NSError *error = nil;
        NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:&error];
        
        if (!error && JSONDictionary) {
            
            for (NSString* key in JSONDictionary) {
                
                if ([key isEqualToString:@"id"]) {
                    [self setValue:[JSONDictionary valueForKey:key] forKey:@"pid"];
                } else if ([key isEqualToString:@"status"]) {
                    [self statusByString:[JSONDictionary valueForKey:key]];
                } else if ([key isEqualToString:@"type"]) {
                    [self typeByString:[JSONDictionary valueForKey:key]];
                } else {
                    [self setValue:[JSONDictionary valueForKey:key] forKey:key];
                }
                
            }
            
        }
    }
    return self;
}

- (void)statusByString:(NSString *)str {
    if ([str isEqualToString:@"FINISHED"]) {
        [self setStatus:FINISHED];
    } else if ([str isEqualToString:@"RUNNING"]) {
        [self setStatus:RUNNING];
    } else {
        [self setStatus:ABORTED];
    }
}

- (void)typeByString:(NSString *)str {
    if ([str isEqualToString:@"WORK"]) {
        [self setType:WORK];
    } else {
        [self setType:REST];
    }
}

- (NSString *)toJSON {
    //НЕКОРРЕКТНО
    unsigned int propertyCount = 0;
    objc_property_t * properties = class_copyPropertyList([self class], &propertyCount);
    
    NSMutableDictionary * propertyNames = [NSMutableDictionary dictionary];
    for (unsigned int i = 0; i < propertyCount; ++i) {
        objc_property_t property = properties[i];
        NSString *key = [NSString stringWithUTF8String:property_getName(property)];
        [propertyNames setValue:[self valueForKey:key] forKey:key];
    }
    free(properties);
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:propertyNames
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return @"{}";
    } else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return nil;
}

@end

//
//  NoteService.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "NoteService.h"
#import "UrlConst.h"
#import "Note.h"

#define MAINPATHNOTE @"/note"

@implementation NoteService

+ (NoteDto *)createNote:(NSString *)payload {
    NSString *path = @"/create";
    
    NoteDto *note = nil;
    
    NSString *strRequest = [NSString stringWithFormat:@"payload=%@", payload];
    NSLog(@"%@", strRequest);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASEURL, MAINPATHNOTE, path]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: [strRequest dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data.length > 0 && error == nil)
    {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        note = [[NoteDto alloc] initWithJSONString:newStr];
    } else {
        NSLog(@"Connection error: %@", error.description);
    }
    
    return note;
}

+ (NSArray *)notes {
    
    NSMutableArray *notes = [[NSMutableArray alloc] init];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", BASEURL, MAINPATHNOTE]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data.length > 0 && error == nil)
    {
        if(NSClassFromString(@"NSJSONSerialization"))
        {
            NSError *serError = nil;
            id object = [NSJSONSerialization JSONObjectWithData:data
                                                        options:0
                                                          error:&serError];
            //NSLog(@"json - %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            NSArray *notesDictionary = object;
            for (NSDictionary *dict in notesDictionary) {
                [notes addObject:[[NoteDto alloc] initWithDictionary:dict]];
            }
        }
    } else {
        NSLog(@"Connection error: %@", error.description);
    }
    
    return (NSArray *)notes;
}

+ (NoteDto *)update:(Note *)noteParam {
    NSString *path = @"/update";
    
    NoteDto *note = nil;
    
    NSString *isCompleted;
    
    if (noteParam.isCompleted)isCompleted = @"true";
    else isCompleted = @"false";
    
    NSString *strRequest = [NSString stringWithFormat:@"id=%@&payload=%@&isCompleted=%@", noteParam.pid, noteParam.payload, isCompleted];
    NSLog(@"%@", strRequest);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASEURL, MAINPATHNOTE, path]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: [strRequest dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data.length > 0 && error == nil)
    {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        note = [[NoteDto alloc] initWithJSONString:newStr];
    } else {
        NSLog(@"Connection error: %@", error.description);
    }
    
    return note;
}

@end

//
//  Pomidorka.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PomidorkaDto.h"
#import "Enums.h"

@interface Pomidorka : NSObject {
    NSString *pid;
    NSDate *serverTs;
    NSDate *startTs;
    NSDate *stopTs;
    TypeDto type;
    StatusDto status;
}

@property NSString *pid;
@property NSDate *serverTs;
@property NSDate *startTs;
@property NSDate *stopTs;
@property TypeDto type;
@property StatusDto status;

- (id)initWithDto:(PomidorkaDto *)pomidorka;

- (double)howMuchTimeLeft;

@end

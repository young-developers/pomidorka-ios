//
//  Note.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NoteDto.h"

@interface Note : NSObject {
    NSString *pid;
    bool isCompleted;
    NSDate *createTs;
    NSDate *lastUpdateTs;
    NSString *payload;
}

@property NSString *pid;
@property bool isCompleted;
@property NSDate *createTs;
@property NSDate *lastUpdateTs;
@property NSString *payload;

- (id)initWithParams:(NSString *)idParam
                 and:(bool)isCompletedParam
                 and:(long)createTsParam
                 and:(long)lastUpdateTsParam
                 and:(NSString *)payloadParam;

- (id)initWithNoteDto:(NoteDto *)note;

@end

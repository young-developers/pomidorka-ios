//
//  PomidorkaService.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "PomidorkaService.h"
#import "UrlConst.h"

#define MAINPATH @"/pomidorka"

@implementation PomidorkaService

+ (PomidorkaDto *)start:(PomidorkaDto *)pomidorka {
    NSString *startPath = @"/start";
    
    /*NSURLComponents *components = [[NSURLComponents alloc] init];
    [components setHost:BASEHOST];
    [components setHost:BASEURL];
    [components setPath:[NSString stringWithFormat:@"%@%@%@",]];*/
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASEURL, MAINPATH, startPath]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                               NSData *data, NSError *connectionError)
     {
         if (data.length > 0 && connectionError == nil)
         {
             NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"%@", newStr);
             /*if(NSClassFromString(@"NSJSONSerialization"))
             {
                 id object = [NSJSONSerialization JSONObjectWithData:data
                                                             options:0
                                                               error:&connectionError];
                 
             }*/
         }
     }];
    
    return nil;
}

+ (PomidorkaDto *)current {
    NSString *startPath = @"/status";
    
    PomidorkaDto *pomidorka = nil;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASEURL, MAINPATH, startPath]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data.length > 0 && error == nil)
    {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        pomidorka = [[PomidorkaDto alloc] initWithJSONString:newStr];
    } else {
        NSLog(@"Connection error: %@", error.description);
    }
    
    return pomidorka;
}

+ (PomidorkaDto *)abort:(NSString *)pid {
    NSString *startPath = @"/abort";
    
    PomidorkaDto *pomidorka = nil;
    
    NSString *strRequest = [NSString stringWithFormat:@"%@", pid];
    NSLog(@"%@", strRequest);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", BASEURL, MAINPATH, startPath]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody: [strRequest dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse * response = nil;
    NSError * error = nil;
    NSData * data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (data.length > 0 && error == nil)
    {
        NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        pomidorka = [[PomidorkaDto alloc] initWithJSONString:newStr];
    } else {
        NSLog(@"Connection error: %@", error.description);
    }
    
    return pomidorka;
}

@end

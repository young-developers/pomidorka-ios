//
//  NoteDto.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "NoteDto.h"

@implementation NoteDto
@synthesize pid, isCompleted, createTs, lastUpdateTs, payload;

- (instancetype)initWithJSONString:(NSString *)JSONString {
    self = [super init];
    if (self) {
        
        NSError *error = nil;
        NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *JSONDictionary = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:&error];
        
        if (!error && JSONDictionary) {
            
            for (NSString* key in JSONDictionary) {
                
                if (![key isEqualToString:@"id"]) {
                    if (![key isEqualToString:@"isCompleted"]) {
                        [self setValue:[JSONDictionary valueForKey:key] forKey:key];
                    } else {
                        if ([[JSONDictionary valueForKey:key] isEqual:@"true"])  {
                            [self setIsCompleted:true];
                        } else {
                            [self setIsCompleted:false];
                        }
                    }
                } else {
                    [self setValue:[JSONDictionary valueForKey:key] forKey:@"pid"];
                }
                
            }
            
        }
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)JSONDictionary {
    self = [super init];
    if (self) {
        for (NSString* key in JSONDictionary) {
            
            if (![key isEqualToString:@"id"]) {
                [self setValue:[JSONDictionary valueForKey:key] forKey:key];
            } else {
                [self setValue:[JSONDictionary valueForKey:key] forKey:@"pid"];
            }
            
        }
    }
    return self;
}

@end

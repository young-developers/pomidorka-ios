//
//  PomidorkaService.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PomidorkaDto.h"

@interface PomidorkaService : NSObject

+ (PomidorkaDto *)start:(PomidorkaDto *)pomidorka;
+ (PomidorkaDto *)current;
+ (PomidorkaDto *)abort:(NSString *)pid;

@end

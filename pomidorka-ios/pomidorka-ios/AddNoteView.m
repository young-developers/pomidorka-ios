//
//  AddNoteView.m
//  pomidorka-ios
//
//  Created by Ivan Babkin on 30.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#import "AddNoteView.h"
//#import "NoteService.m"

@implementation AddNoteView
@synthesize noteField, myDelegate;

- (void) showAlertWithTitle: (NSString*) title message: (NSString*) message
{
    UIAlertView* alert= [[UIAlertView alloc] initWithTitle: title message: message
                                                  delegate: NULL cancelButtonTitle: @"OK" otherButtonTitles: NULL];
    [alert show];
    
}

- (id)initWithDelegate:(id <AddNoteDelegate>)deleg {
    self = [super init];
    if ( self ) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"AddNoteView"
                                              owner:self
                                            options:nil] lastObject];
        myDelegate = deleg;
    }
    return self;
}

- (IBAction)ok:(id)sender {
    if ([noteField.text isEqual:@""]) {
        [self showAlertWithTitle:@"Ошибка" message:@"Введите заметку"];
        return;
    } else {
        [myDelegate addNoteFromView:noteField.text];
        [myDelegate loadNotes];
        self.hidden = YES;
        [self removeFromSuperview];
    }
}
@end

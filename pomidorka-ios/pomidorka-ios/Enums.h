//
//  Enums.h
//  pomidorka-ios
//
//  Created by Ivan Babkin on 29.10.16.
//  Copyright © 2016 Ivan Babkin. All rights reserved.
//

#ifndef Enums_h
#define Enums_h

typedef enum{
    WORK,
    REST
}TypeDto;

typedef enum {
    RUNNING,
    ABORTED,
    FINISHED
}StatusDto;

#endif /* Enums_h */
